import sys

import pyexamp

if "--strict" in sys.argv:
    sys.argv.remove("--strict")
    tester = pyexamp.teststrict
else:
    tester = pyexamp.test

sys.exit(tester(*sys.argv[1:]))
